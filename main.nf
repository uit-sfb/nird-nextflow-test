#!/usr/bin/env nextflow

cheers = Channel.from 'Bonjour'

process sayHello {
  echo true

  input:
    val x from cheers
  script:
    """
    echo '$x world!'
    id
    ls -la /mount/path/marcadella
    """
}
