## Deployment

## Service deployment

- Login to NIRD: `kubed -renew nird`
- `kubectl apply -f deployment/api.yaml` (or to simply update the image: `kubectl delete pod xxx`)

The UI should then be available here: https://staging.nf.metapipe.metakube.sigma2.no/

### Ubuntu

The ubuntu deployment may be useful to run `kubectl cp` as the version of `tar` installed on nextflow image is not able to work with tar files.