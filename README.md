## Running the example

Login to NIRD:
`kubed -renew nird`

Create a nextflow pod (useful to list the volume content): `nextflow kuberun login`  
Note: `nextflow.config` is used to mount the volumes osv, so it matters in which directory you run the command.

Run hello pipeline:
`nextflow kuberun -latest https://gitlab.com/Marcadella/nird-nextflow-test`

Note: it is not possible to run `nextflow run .` since the executor is set to `k8s` in the configuration.

## Logs

See https://www.nextflow.io/docs/latest/tracing.html#execution-log

To delete, see `nextflow clean -h`.