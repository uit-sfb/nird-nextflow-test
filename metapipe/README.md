## Run metapipe

### Upload data

- Login to NIRD: `kubed -renew nird`
- Start ubuntu deployment: `kubectl apply -f deployment/ubuntu.yaml`  
- `kubectl cp` input to `/mount/path/marcadella/input` data using `nextflow kuberun login`.  
  Ex: `kubectl cp  /home/marcadella/IdeaProjects/metapipe/test/resources/datasets/default_reads_fastq/forward.fastq.gz  metakube-ns9640k-ubuntu-<containerId>:/mount/path/marcadella/input/forward.fastq.gz`
- Stop ubuntu deployment

### From local machine

- `cd metapipe/<sub-dir>`
- `nextflow -c nextflow.config kuberun -latest https://gitlab.com/uit-sfb/metapipe -with-report`

The outputs will be available in the `outputs` dir when logged in (`nextflow kuberun login`).

Notes:
- no report or log will be available from the client
- `-with-trace` and `-with-timeline` are not working with `kuberun` 
- run `nextflow log` within `nextflow kuberun login`

### From service container

- `kubectl exec -it <container_id> bash`
- `cd /mount/path/marcadella`
- `nextflow -c nextflow.config run -latest https://gitlab.com/uit-sfb/metapipe -with-report -with-trace -with-timeline`

Notes:
- the easiest way to update nextflow.config is to use the first method.
- Use [nextflow log](https://www.nextflow.io/docs/latest/tracing.html) to get the run details