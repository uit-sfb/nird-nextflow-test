package no.uit.sfb.facade.bootstrap.grid

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined, |}

object Container {

  class Props(val className: UndefOr[String] = undefined,
              val fluid: UndefOr[Boolean | String] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "Container")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(className: String = null, fluid: Boolean | String = false)(
      content: VdomNode*): VdomElement = {
    componentJS(new Props(className, fluid))(content: _*)
  }
}
