package no.uit.sfb.facade.bootstrap.form

import japgolly.scalajs.react.Ref.Handle
import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.raw.React.RefHandle
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}
import org.scalajs.dom.html

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

object UncontrolledFormControl {

  class Props(
      val as: UndefOr[String] = undefined,
      //val custom : Boolean = null,
      val defaultValue: UndefOr[String] = undefined,
      /*val disabled: Boolean = false,
                               val htmlSize: Int = null,
                               val id: String = null,
                               val isInvalid: Boolean = false,
                               val isValid: Boolean = false,*/
      val placeholder: UndefOr[String] = undefined,
      /*val plaintext: Boolean = null,
                                 val readOnly: Boolean = null,
                                 val size: String = null,*/
      val ref: RefHandle[html.Input],
      val `type`: UndefOr[String] = undefined,
  ) extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Form.RawComponent.Control)

  def apply(
      ref: Handle[html.Input],
      as: String = "input",
      defaultValue: UndefOr[String] = undefined,
      placeholder: UndefOr[String] = undefined,
      `type`: UndefOr[String] = undefined)(content: VdomNode*): VdomElement =
    componentJS(
      new Props(as, defaultValue, placeholder, ref.raw, `type`)
    )(content: _*)
}
