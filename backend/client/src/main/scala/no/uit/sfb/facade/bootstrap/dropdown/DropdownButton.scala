package no.uit.sfb.facade.bootstrap.dropdown

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object DropdownButton {

  class Props(val disabled: UndefOr[Boolean] = undefined,
              val drop: UndefOr[String] = undefined,
              val href: UndefOr[String] = undefined,
              val id: String, //Compulsory
              //val menuAlign: UndefOr[String] = undefined,
              val onClick: UndefOr[js.Function0[Unit]] = undefined,
              val size: UndefOr[String] = undefined,
              val title: String, //compulsory
              val variant: UndefOr[String] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "DropdownButton")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
      id: String, //Compulsory
      title: String, //compulsory,
      drop: String = "down",
      disabled: Boolean = false,
      href: UndefOr[String] = undefined,
      //menuAlign: UndefOr[String = null,
      onClick: UndefOr[js.Function0[Unit]] = undefined,
      size: UndefOr[String] = undefined,
      variant: UndefOr[String] = undefined)(content: VdomNode*): VdomElement = {
    componentJS(
      new Props(disabled,
                drop,
                href,
                id,
                //menuAlign,
                onClick,
                size,
                title,
                variant))(content: _*)
  }
}
