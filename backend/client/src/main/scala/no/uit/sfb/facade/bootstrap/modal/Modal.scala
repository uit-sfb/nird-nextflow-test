package no.uit.sfb.facade.bootstrap.modal

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Callback, Children, CtorType, JsComponent, ReactEventFromInput}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined, |}

object Modal {

  class Props(
      val animation: UndefOr[Boolean] = undefined,
      val autoFocus: UndefOr[Boolean] = undefined,
      val backdrop: UndefOr[Boolean | String] = undefined, //true, false, "static"
      val centered: UndefOr[Boolean] = undefined,
      val contentClassName: UndefOr[String] = undefined,
      val dialogClassName: UndefOr[String] = undefined,
      val enforceFocus: UndefOr[Boolean] = undefined,
      val keyboard: UndefOr[Boolean] = undefined,
      val onEnter: UndefOr[js.Function0[Unit]] = undefined,
      val onEntered: UndefOr[js.Function0[Unit]] = undefined,
      val onEntering: UndefOr[js.Function0[Unit]] = undefined,
      val onEscapeKeyDown: UndefOr[js.Function0[Unit]] = undefined,
      val onExit: UndefOr[js.Function0[Unit]] = undefined,
      val onExited: UndefOr[js.Function0[Unit]] = undefined,
      val onExiting: UndefOr[js.Function0[Unit]] = undefined,
      val onHide: UndefOr[js.Function0[Unit]] = undefined,
      val onShow: UndefOr[js.Function0[Unit]] = undefined,
      val restoreFocus: UndefOr[Boolean] = undefined,
      val scrollable: UndefOr[Boolean] = undefined,
      val show: UndefOr[Boolean] = undefined,
      val size: UndefOr[String] = undefined, //"sm", "lg", "xl"
  ) extends js.Object

  @JSImport("react-bootstrap", "Modal")
  @js.native
  object RawComponent extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(animation: Boolean = true,
  autoFocus: Boolean = true,
  backdrop: Boolean | String = true, //true, false, "static"
  centered: Boolean = false,
  contentClassName: UndefOr[String] = undefined,
  dialogClassName: UndefOr[String] = undefined,
  enforceFocus: Boolean = true,
  keyboard: Boolean = true,
  onEnter: Callback = Callback.empty,
  onEntered: Callback = Callback.empty,
  onEntering: Callback = Callback.empty,
  onEscapeKeyDown: Callback = Callback.empty,
  onExit: Callback = Callback.empty,
  onExited: Callback = Callback.empty,
  onExiting: Callback = Callback.empty,
  onHide: Callback = Callback.empty,
  onShow: Callback = Callback.empty,
  restoreFocus: Boolean = true,
  scrollable: Boolean = false,
  show: Boolean = false,
  size: String = "lg")(content: VdomNode*): VdomElement =
    componentJS(new Props(
      animation,
      autoFocus,
      backdrop,
      centered,
      contentClassName,
      dialogClassName,
      enforceFocus,
      keyboard,
      onEnter.toJsCallback,
      onEntered.toJsCallback,
      onEntering.toJsCallback,
      onEscapeKeyDown.toJsCallback,
      onExit.toJsCallback,
      onExited.toJsCallback,
      onExiting.toJsCallback,
      onHide.toJsCallback,
      onShow.toJsCallback,
      restoreFocus,
      scrollable,
      show,
      size,
      )
    )(content: _*)
}
