package no.uit.sfb.facade.bootstrap.form

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined, |}

object FormLabel {

  class Props(val column: UndefOr[Boolean | String] = undefined)
      extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Form.RawComponent.Label)

  def apply(column: Boolean | String = false)(content: VdomNode*): VdomElement =
    componentJS(new Props(column))(content: _*)
}
