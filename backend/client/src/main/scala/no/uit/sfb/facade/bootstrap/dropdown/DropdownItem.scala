package no.uit.sfb.facade.bootstrap.dropdown

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Callback, Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

object DropdownItem {

  class Props(val active: UndefOr[Boolean] = undefined,
              val disabled: UndefOr[Boolean] = undefined,
              val download: UndefOr[String] = undefined,
              val eventKey: UndefOr[String] = undefined,
              val href: UndefOr[String] = undefined,
              val onClick: UndefOr[js.Function0[Unit]] = undefined)
      extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Dropdown.RawComponent.Item)

  def apply(
      active: Boolean = false,
      disabled: Boolean = false,
      download: UndefOr[String] = undefined,
      eventKey: UndefOr[String] = undefined,
      href: UndefOr[String] = undefined,
      onClick: Callback = Callback.empty)(content: VdomNode*): VdomElement = {
    componentJS(
      new Props(active,
                disabled,
                download,
                eventKey,
                href,
                onClick.toJsCallback))(content: _*)
  }
}
