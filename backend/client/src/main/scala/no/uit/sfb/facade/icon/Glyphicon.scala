package no.uit.sfb.facade.icon

import japgolly.scalajs.react.vdom.VdomElement
import japgolly.scalajs.react.{Children, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

//From https://react-icons.github.io/

object Glyphicon {

  class Props() extends js.Object

  @JSImport("react-icons/io", "IoIosGlobe")
  @js.native
  object IoIosGlobe extends js.Object

  @JSImport("react-icons/io", "IoMdGlobe")
  @js.native
  object IoMdGlobe extends js.Object

  @JSImport("react-icons/bs", "BsCaretLeft")
  @js.native
  object BsCaretLeft extends js.Object

  @JSImport("react-icons/bs", "BsCaretRight")
  @js.native
  object BsCaretRight extends js.Object

  @JSImport("react-icons/md", "MdViewCarousel")
  @js.native
  object MdViewCarousel extends js.Object

  @JSImport("react-icons/md", "MdViewList")
  @js.native
  object MdViewList extends js.Object

  @JSImport("react-icons/go", "GoGraph")
  @js.native
  object GoGraph extends js.Object

  @JSImport("react-icons/go", "GoInfo")
  @js.native
  object GoInfo extends js.Object

  @JSImport("react-icons/bs", "BsInfoSquare")
  @js.native
  object BsInfoSquare extends js.Object

  @JSImport("react-icons/ai", "AiOutlineQuestionCircle")
  @js.native
  object AiOutlineQuestionCircle extends js.Object

  @JSImport("react-icons/md", "MdAnnouncement")
  @js.native
  object MdAnnouncement extends js.Object

  @JSImport("react-icons/gr", "GrNext")
  @js.native
  object GrNext extends js.Object

  @JSImport("react-icons/ai", "AiOutlineCloudDownload")
  @js.native
  object AiOutlineCloudDownload extends js.Object

  @JSImport("react-icons/hi", "HiDocumentDownload")
  @js.native
  object HiDocumentDownload extends js.Object

  @JSImport("react-icons/io5", "IoOptionsOutline")
  @js.native
  object IoOptionsOutline extends js.Object

  @JSImport("react-icons/ai", "AiOutlineFunction")
  @js.native
  object AiOutlineFunction extends js.Object

  @JSImport("react-icons/bs", "BsClipboardData")
  @js.native
  object BsClipboardData extends js.Object

  @JSImport("react-icons/ai", "AiOutlineSetting")
  @js.native
  object AiOutlineSetting extends js.Object

  @JSImport("react-icons/bi", "BiColumns")
  @js.native
  object BiColumns extends js.Object

  private lazy val resources = Map(
    "PreviousPage" -> BsCaretLeft,
    "NextPage" -> BsCaretRight,
    "ViewCarousel" -> MdViewCarousel,
    "ViewList" -> MdViewList,
    "Graph" -> GoGraph,
    "Info" -> GoInfo,
    "Info2" -> BsInfoSquare,
    "Help" -> AiOutlineQuestionCircle,
    "Announcement" -> MdAnnouncement,
    "Next" -> GrNext,
    "CloudDownload" -> AiOutlineCloudDownload,
    "DocumentDownload" -> HiDocumentDownload,
    "Options" -> IoOptionsOutline,
    "Settings" -> AiOutlineSetting,
    "Columns" -> BiColumns,
    "Function" -> AiOutlineFunction,
    "Data" -> BsClipboardData,
    "Globe" -> IoIosGlobe,
    "GeoGlobe" -> IoMdGlobe,
  ).view.mapValues { obj =>
    val comp = JsComponent[Props, Children.Varargs, Null](obj)
    comp(new Props())()
  }

  def apply(glyph: String): VdomElement = {
    resources(glyph)
  }
}
