package no.uit.sfb.facade.bootstrap.pagination

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object Pagination {

  class Props(
      val size: UndefOr[String] = undefined
  ) extends js.Object

  @JSImport("react-bootstrap", "Pagination")
  @js.native
  object RawComponent extends js.Object {
    val First: js.Object = js.native
    val Prev: js.Object = js.native
    val Next: js.Object = js.native
    val Last: js.Object = js.native
    val Ellipsis: js.Object = js.native
  }

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(size: UndefOr[String] = undefined)(
      content: VdomNode*): VdomElement =
    componentJS(new Props(size))(content: _*)
}
