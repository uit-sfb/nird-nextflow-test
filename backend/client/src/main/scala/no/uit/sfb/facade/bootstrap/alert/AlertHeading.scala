package no.uit.sfb.facade.bootstrap.alert

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js

object AlertHeading {

  class Props() extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Alert.RawComponent.Heading)

  def apply()(content: VdomNode*): VdomElement =
    componentJS(new Props())(content: _*)
}
