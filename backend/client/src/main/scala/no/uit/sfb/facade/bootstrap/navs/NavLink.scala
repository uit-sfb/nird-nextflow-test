package no.uit.sfb.facade.bootstrap.navs

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Callback, Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

object NavLink {

  class Props(val active: UndefOr[Boolean] = undefined,
              val disabled: UndefOr[Boolean] = undefined,
              val eventKey: UndefOr[String] = undefined,
              val href: UndefOr[String] = undefined,
              val onSelect: UndefOr[js.Function1[String, Unit]] = undefined)
      extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Nav.RawComponent.Link)

  def apply(active: Boolean = false,
            disabled: Boolean = false,
            eventKey: UndefOr[String] = undefined,
            href: UndefOr[String] = undefined,
            onSelect: String => Callback = _ => Callback.empty)(
      content: VdomNode*): VdomElement =
    componentJS(
      new Props(active,
                disabled,
                eventKey,
                href,
                js.defined(onSelect(_).runNow())))(content: _*)
}
