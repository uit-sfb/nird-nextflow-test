package no.uit.sfb.facade.bootstrap.buttons

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{
  Callback,
  Children,
  CtorType,
  JsComponent,
  ReactEventFromInput
}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object ToggleButton {

  class Props(
      val checked: UndefOr[Boolean] = undefined,
      val disabled: UndefOr[Boolean] = undefined,
      val name: UndefOr[String] = undefined,
      val onChange: UndefOr[js.Function1[ReactEventFromInput, Unit]] = undefined,
      val `type`: UndefOr[String] = undefined,
      val value: UndefOr[String] = undefined
  ) extends js.Object

  @JSImport("react-bootstrap", "ToggleButton")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(
      checked: Boolean = false,
      disabled: Boolean = false,
      name: UndefOr[String] = undefined,
      onChange: ReactEventFromInput => Callback = _ => Callback.empty,
      `type`: UndefOr[String] = undefined,
      value: UndefOr[String] = undefined)(content: VdomNode*): VdomElement = {
    componentJS(
      new Props(checked,
                disabled,
                name,
                js.defined(onChange(_).runNow()),
                `type`,
                value))(content: _*)
  }
}
