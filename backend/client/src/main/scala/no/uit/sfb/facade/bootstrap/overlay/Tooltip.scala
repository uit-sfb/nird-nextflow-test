package no.uit.sfb.facade.bootstrap.overlay

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

object Tooltip {

  class Props() extends js.Object

  @JSImport("react-bootstrap", "Tooltip")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply()(content: VdomNode*): VdomElement = {
    componentJS(new Props())(content: _*)
  }

  def text(tooltipText: String): Option[VdomElement] = {
    if (tooltipText.isEmpty)
      None
    else
      Some(componentJS(new Props())(tooltipText))
  }

  def text(tooltipText: Option[String]): Option[VdomElement] = {
    text(tooltipText.getOrElse(""))
  }
}
