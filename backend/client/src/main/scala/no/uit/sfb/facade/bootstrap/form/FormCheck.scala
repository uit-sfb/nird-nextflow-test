package no.uit.sfb.facade.bootstrap.form

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Callback, Children, CtorType, JsComponent, ReactEventFromInput}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined, |}
import scala.util.Random

object FormCheck {

  class Props(
      val checked: UndefOr[Boolean] = undefined,
      val disabled: UndefOr[Boolean] = undefined,
      val feedbackTooltip: UndefOr[Boolean] = undefined,
      val id: UndefOr[String] = undefined,
      val inline: UndefOr[Boolean] = undefined,
      val isInvalid: UndefOr[Boolean] = undefined,
      val isValid: UndefOr[Boolean] = undefined,
      val label: UndefOr[String] = undefined,
      val onClick: UndefOr[js.Function1[ReactEventFromInput, Unit]] = undefined,
      val onChange: UndefOr[js.Function1[ReactEventFromInput, Unit]] = undefined,
      val title: UndefOr[String] = undefined,
      val `type`: UndefOr[String] = undefined, //radio | checkbox | switch
  ) extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Form.RawComponent.Check)

  def apply(
           checked: Boolean = false,
      disabled: Boolean = false,
      feedbackTooltip: Boolean = false,
      id: String = Random.alphanumeric.take(5).mkString,
      inline: Boolean = false,
      isInvalid: Boolean = false,
      isValid: Boolean = false,
      label: String = "",
      onClick: ReactEventFromInput => Callback = _ => Callback.empty,
      onChange: ReactEventFromInput => Callback = _ => Callback.empty,
      title: String = "",
      `type`: String = "checkbox",
  )(content: VdomNode*): VdomElement =
    componentJS(
      new Props(
        checked,
        disabled,
                feedbackTooltip,
                id,
                inline,
                isInvalid,
                isValid,
                label,
                js.defined(onClick(_).runNow()),
                js.defined(onChange(_).runNow()),
                title,
                `type`
      ))(content: _*)
}
