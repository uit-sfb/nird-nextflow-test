package no.uit.sfb.facade.bootstrap.grid

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined, |}

object Col {

  class Props(
      val lg: UndefOr[Int | String] = undefined,
      val md: UndefOr[Int | String] = undefined,
      val sm: UndefOr[Int | String] = undefined,
      val xl: UndefOr[Int | String] = undefined,
      val xs: UndefOr[Int | String] = undefined,
      val className: UndefOr[String] = undefined,
  ) extends js.Object

  @JSImport("react-bootstrap", "Col")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(lg: UndefOr[Int | String] = undefined,
            md: UndefOr[Int | String] = undefined,
            sm: UndefOr[Int | String] = undefined,
            xl: UndefOr[Int | String] = undefined,
            xs: UndefOr[Int | String] = undefined,
            className: UndefOr[String] = undefined)(
      content: VdomNode*): VdomElement = {
    componentJS(new Props(lg, md, sm, xl, xs, className))(content: _*)
  }
}
