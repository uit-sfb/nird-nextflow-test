package no.uit.sfb.facade.bootstrap.dropdown

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

object DropdownMenu {

  class Props(val align: UndefOr[String] = undefined) extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Dropdown.RawComponent.Menu)

  def apply(
      align: String = "left"
  )(content: VdomNode*): VdomElement = {
    componentJS(new Props(align))(content: _*)
  }
}
