package no.uit.sfb.facade.bootstrap.form

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{
  Callback,
  Children,
  CtorType,
  JsComponent,
  ReactEventFromInput
}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object Form {

  class Props(
      //val as: String,
      val inline: UndefOr[Boolean] = undefined,
      val onSubmit: UndefOr[js.Function1[ReactEventFromInput, Unit]] = undefined,
      val validated: UndefOr[Boolean] = undefined
  ) extends js.Object

  @JSImport("react-bootstrap", "Form")
  @js.native
  object RawComponent extends js.Object {
    val Group: js.Object = js.native
    val Control: js.Object = js.native
    val Label: js.Object = js.native
    val Check: js.Object = js.native
  }

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(inline: Boolean = false,
            onSubmit: ReactEventFromInput => Callback = _ => Callback.empty,
            validated: Boolean = false)(content: VdomNode*): VdomElement =
    componentJS(new Props(inline, js.defined(onSubmit(_).runNow()), validated))(
      content: _*)
}
