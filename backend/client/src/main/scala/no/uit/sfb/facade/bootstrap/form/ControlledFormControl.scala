package no.uit.sfb.facade.bootstrap.form

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{
  Callback,
  Children,
  CtorType,
  JsComponent,
  ReactEventFromInput,
  ReactKeyboardEventFromInput
}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}

object ControlledFormControl {

  class Props(
      val as: UndefOr[String] = undefined,
      /*val custom : UndefOr[Boolean] = undefined,
                      val disabled: UndefOr[Boolean] = undefined,
                      val htmlSize: UndefOr[Int] = undefined,
                      val id: UndefOr[String] = undefined,
                      val isInvalid: UndefOr[Boolean] = undefined,
                      val isValid: UndefOr[Boolean] = undefined,*/
      val onChange: UndefOr[js.Function1[ReactEventFromInput, Unit]] = undefined,
      val onKeyDown: UndefOr[js.Function1[ReactKeyboardEventFromInput, Unit]] =
        undefined,
      val placeholder: UndefOr[String] = undefined,
      /*val plaintext: UndefOr[Boolean] = undefined,
                        val readOnly: UndefOr[Boolean] = undefined,
                        val size: UndefOr[String] = undefined,*/
      val `type`: UndefOr[String] = undefined,
      //val defaultValue: UndefOr[String] = undefined,
      val value: UndefOr[String] = undefined
  ) extends js.Object

  val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](Form.RawComponent.Control)

  def apply(as: String = "input",
            onChange: ReactEventFromInput => Callback = _ => Callback.empty,
            onKeyDown: ReactKeyboardEventFromInput => Callback = _ =>
              Callback.empty, //Use ev.keyCode
            placeholder: String = "",
            //defaultValue: String = "",
            value: String = "",
            `type`: String = "")(content: VdomNode*): VdomElement =
    componentJS(
      new Props(as,
                js.defined(onChange(_).runNow()),
                js.defined(onKeyDown(_).runNow()),
                placeholder,
                `type`,
                //defaultValue,
                value)
    )(content: _*)
}
