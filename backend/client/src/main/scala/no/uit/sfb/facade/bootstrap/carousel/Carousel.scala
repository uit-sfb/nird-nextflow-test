package no.uit.sfb.facade.bootstrap.carousel

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js.{UndefOr, undefined}

object Carousel {

  class Props(val fade: UndefOr[Boolean] = undefined,
              val indicators: UndefOr[Boolean] = undefined,
              val interval: UndefOr[Int] = undefined, //Use null to not cycle
              val keyboard: UndefOr[Boolean] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "Carousel")
  @js.native
  object RawComponent extends js.Object {
    val Item: js.Object = js.native
    val Caption: js.Object = js.native
  }

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(fade: Boolean = false,
            indicators: Boolean = true,
            interval: Int = 5000, //Use null to not cycle
            keyboard: Boolean = true)(content: VdomNode*): VdomElement = {
    componentJS(new Props(fade, indicators, interval, keyboard))(content: _*)
  }
}
