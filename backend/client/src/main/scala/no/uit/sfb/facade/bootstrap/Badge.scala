package no.uit.sfb.facade.bootstrap

import japgolly.scalajs.react.component.Js.Component
import japgolly.scalajs.react.vdom.{VdomElement, VdomNode}
import japgolly.scalajs.react.{Children, CtorType, JsComponent}

import scala.scalajs.js
import scala.scalajs.js.{UndefOr, undefined}
import scala.scalajs.js.annotation.JSImport

//Facade for https://react-bootstrap.github.io/components/badge/

object Badge {

  class Props(val pill: UndefOr[Boolean] = undefined,
              val variant: UndefOr[String] = undefined)
      extends js.Object

  @JSImport("react-bootstrap", "Badge")
  @js.native
  object RawComponent extends js.Object

  private val componentJS: Component[Props, Null, CtorType.PropsAndChildren] =
    JsComponent[Props, Children.Varargs, Null](RawComponent)

  def apply(pill: Boolean = false, variant: String = "dark")(
      content: VdomNode*): VdomElement = {
    componentJS(new Props(pill, variant))(content: _*)
  }
}
