package no.uit.sfb.facade

import scala.scalajs.js

object OptionToUndefOr {
  implicit def optionToUndefOr[A](o: Option[A]): js.UndefOr[A] = {
    o match {
      case Some(a) => js.defined(a)
      case None => js.undefined
    }
  }
}
