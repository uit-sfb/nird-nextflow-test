# Backend

## Requirements

- Java 11 or later
- NPM & NodeJS 12 or later
- SBT

## Getting started

Run `sbt run` (from this location) and the UI should be available at `http://localhost:9000`.
SBT is able to detect code change and compile on-the-fly.

## Notes about the bundling process

Steps:

- scalaJS compilation (scala -> JS): [sbt-scalajs plugin](http://www.scala-js.org/doc/sbt-plugin.html)
- JS transpilation and bundling: [scalajs-bundler](https://scalacenter.github.io/scalajs-bundler/)

Transpilation is performed by [webpack](https://webpack.js.org/concepts/) loaders (ts-loader, babel-loader,
source-map-loader, ...) and consists in converting flavors of JS -> plain old JS. In our case, we use `babel-loader`
followed by `source-map-loader`. The former comes from our custom webpack config (`webpack.config.js`), while the latter
is automatically added by the plugin to the default webpack config.

In terms of commands:

- To compile scala -> js: `client/fastLinkJS`.
- To transpile/bundle the code after compilation: `client/fastOptJS::webpack`.

