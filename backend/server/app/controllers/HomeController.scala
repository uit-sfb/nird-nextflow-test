package controllers

import javax.inject._
import play.api.Configuration
import play.api.mvc._
import no.uit.sfb.info.metapipe_backend.BuildInfo
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext
import sys.process._

@Singleton
class HomeController @Inject()(cc: ControllerComponents,
                               implicit val ec: ExecutionContext,
                               implicit val conf: Configuration)
    extends AbstractController(cc) {

  def index = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def info() = Action {
    val res = Map(
      "name" -> BuildInfo.name,
      "ver" -> BuildInfo.version,
      "git" -> BuildInfo.gitCommit
    )
    Ok(Json.toJson(res))
  }

  def testJob(resume: Boolean) = Action {
    val proc = Process(
      Seq(
        "nextflow",
        "-c",
        "nextflow.config",
        "run",
        "-latest",
        if (resume) "-resume" else "",
        "https://gitlab.com/uit-sfb/metapipe",
        "--reads",
        "input/{forward,reverse}.fastq*" // Do NOT add extra quotes
      ).filter(_.nonEmpty),
      new java.io.File("/mount/path/marcadella")
    )
    val retc = proc.!
    if (retc == 0)
      Ok("Done")
    else
      Ok(s"Error code $retc")
  }
}
