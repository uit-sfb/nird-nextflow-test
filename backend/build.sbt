import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport.{dockerBaseImage, dockerRepository, dockerUsername}
import sbt.Keys.libraryDependencies
import sbtbuildinfo.BuildInfoPlugin.autoImport.buildInfoPackage

lazy val backendRoot = project.in(file(".")).
  aggregate(server, client).
  settings(
    publish := {},
    publishLocal := {},
  )

lazy val server = (project in file("server"))
  .settings(commonSettings)
  .settings(
    name := "metapipe-backend",
    scalaJSProjects := Seq(client),
    pipelineStages in Assets := Seq(scalaJSPipeline),
    pipelineStages := Seq(digest, gzip),
    // triggers scalaJSPipeline when using compile or continuous compilation
    compile in Compile := ((compile in Compile) dependsOn scalaJSPipeline).value,
    buildInfoKeys := Seq[BuildInfoKey](
      name,
      version,
      scalaVersion,
      sbtVersion,
      gitCommit
    ),
    buildInfoPackage := s"${organization.value}.info.${name.value.replace('-', '_')}",
    libraryDependencies ++= Seq(
      "com.vmunier" %% "scalajs-scripts" % "1.1.4",
      ws,
      guice,
      specs2 % Test
    )
  )
  .enablePlugins(PlayScala, WebScalaJSBundlerPlugin, BuildInfoPlugin)

lazy val client = (project in file("client"))
  .settings(commonSettings)
  .settings(
    scalaJSUseMainModuleInitializer := false,
    webpackBundlingMode := BundlingMode.LibraryAndApplication(), //https://stackoverflow.com/a/64317357/4965515
    version in webpack := "4.46.0", //Cannot update to 5+
    version in startWebpackDevServer := "3.11.2",
    webpackConfigFile := Some(baseDirectory.value / "webpack.config.js"),
    libraryDependencies ++= Seq(
      "org.scala-js" %%% "scalajs-dom" % "1.1.0",
      "com.github.japgolly.scalajs-react" %%% "core" % "1.7.7",
      "com.github.japgolly.scalajs-react" %%% "extra" % "1.7.7",
      "io.github.cquiroz" %%% "scala-java-time" % "2.1.0"
    ),
    npmDependencies in Compile ++= Seq(
      "react" -> "17.0.1",
      "react-dom" -> "17.0.1",
      "react-bootstrap" -> "1.6.0",
      "react-icons" -> "4.2.0",
      "jsdom" -> "16.5.3",
      "babel-loader" -> "7.1.5", //Needed to transpile reticent libraries (cf webpack.config.js)
      "babel-core" -> "7.0.0-bridge.0", //Needed by babel-loader
      "@babel/preset-env" -> "7.14.2",
      "@babel/core" -> "7.14.2", //Needed by @babel/preset-env
    ),
    npmDependencies in Test ++= Seq(),
    requireJsDomEnv in Test := true
  )
  .enablePlugins(ScalaJSPlugin, ScalaJSBundlerPlugin)

lazy val gitCommit = settingKey[String]("Git SHA")
lazy val gitRefName = settingKey[String]("Git branch or tag")
lazy val isRelease = settingKey[Boolean]("Is release")
lazy val dockerRegistry = settingKey[String]("Docker registry")
lazy val dockerImageName = settingKey[String]("Docker image name")

useJGit

lazy val commonSettings = Seq(
  useCoursier := false,
  scalaVersion := "2.13.4",
  organization := "no.uit.sfb",
  organizationName := "uit-sfb",
  resolvers += "gitlab" at "https://gitlab.com/api/v4/groups/2067095/-/packages/maven",
  gitCommit := {
    sys.env.getOrElse("CI_COMMIT_SHA", "unknown")
  },
  gitRefName := {
    sys.env.getOrElse("CI_COMMIT_REF_NAME", git.gitCurrentBranch.value)
  },
  isRelease := {
    sys.env.getOrElse("CI_COMMIT_REF_PROTECTED", "false").toBoolean && sys.env.getOrElse("CI_COMMIT_TAG", "").nonEmpty
  },
  dockerRegistry := {
    sys.env.getOrElse("CI_REGISTRY", s"registry.gitlab.com")
  },
  dockerImageName := {
    s"${sys.env.getOrElse("CI_REGISTRY_IMAGE", s"${dockerRegistry.value}/${organizationName.value}/sarscovid19db")}"
  },
  version := gitRefName.value, //Do not use slug here as it would replace '.' with '-'
  dockerLabels := Map("gitCommit" -> s"${gitCommit.value}@${gitRefName.value}"), //Do NOT add in Docker
  dockerRepository in Docker := Some(dockerRegistry.value),
  dockerUsername in Docker := Some(dockerImageName.value.split("/").tail.mkString("/")),
  dockerBaseImage := "nextflow/nextflow:latest", //Do NOT add "in Docker"
  //dockerChmodType in Docker := DockerChmodType.UserGroupWriteExecute,
  //dockerPermissionStrategy in Docker := DockerPermissionStrategy.CopyChown,
  //daemonGroup in Docker := "0"
  daemonUser in Docker := "sfb", //"in Docker" needed for this parameter,
  libraryDependencies ++= Seq()
)

onLoad in Global := (onLoad in Global).value.andThen(state => "project server" :: state)

enablePlugins(DockerPlugin, JavaAppPackaging)
